package nom.brunokarpo.test.catalog.resources

import nom.brunokarpo.test.catalog.model.Film
import nom.brunokarpo.test.catalog.model.Hero
import nom.brunokarpo.test.catalog.service.GalaxyService
import org.eclipse.microprofile.graphql.GraphQLApi
import org.eclipse.microprofile.graphql.Source
import javax.inject.Inject

@GraphQLApi
class HeroesResource(
    @Inject private val service: GalaxyService
) {

    fun heroes(@Source film: Film) = service.getHeroesByFilm(film)

    fun multiHeroes(@Source films: Collection<Film>): Collection<Collection<Hero>> {
        return films.map { film -> service.getHeroesByFilm(film) }
    }
}