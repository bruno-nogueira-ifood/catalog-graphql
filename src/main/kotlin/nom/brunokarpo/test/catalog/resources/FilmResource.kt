package nom.brunokarpo.test.catalog.resources

import nom.brunokarpo.test.catalog.service.GalaxyService
import org.eclipse.microprofile.graphql.Description
import org.eclipse.microprofile.graphql.GraphQLApi
import org.eclipse.microprofile.graphql.Name
import org.eclipse.microprofile.graphql.Query
import javax.inject.Inject

@GraphQLApi
class FilmResource(
    @Inject private val service: GalaxyService
) {

    @Query("allFilms")
    @Description("Get all Films from a galaxy far far away")
    fun getAllFilms() = service.getAllFilms()

    @Query
    @Description("Get a Film from a galaxy far far away")
    fun getFilm(@Name("filmId") id: Int) = service.getFilm(id)

}