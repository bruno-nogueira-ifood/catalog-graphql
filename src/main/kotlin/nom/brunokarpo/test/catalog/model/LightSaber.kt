package nom.brunokarpo.test.catalog.model

enum class LightSaber {
    RED, BLUE, GREEN
}