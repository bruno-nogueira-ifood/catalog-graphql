package nom.brunokarpo.test.catalog.model

data class Hero(
    var name: String = "",
    var surname: String = "",
    var height: Double = 0.0,
    var mass: Int = 0,
    var darkSide: Boolean = false,
    var lightSaber: LightSaber? = null,
    var episodeIds: Collection<Int> = emptyList()
)
