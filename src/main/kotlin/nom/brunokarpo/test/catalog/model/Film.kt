package nom.brunokarpo.test.catalog.model

import java.time.LocalDate

data class Film(
    var title: String = "",
    var episodeID: Int = 0,
    var director: String = "",
    var releaseDate: LocalDate = LocalDate.now()
)
