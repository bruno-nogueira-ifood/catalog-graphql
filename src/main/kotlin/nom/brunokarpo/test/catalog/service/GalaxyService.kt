package nom.brunokarpo.test.catalog.service

import nom.brunokarpo.test.catalog.model.Film
import nom.brunokarpo.test.catalog.model.Hero
import nom.brunokarpo.test.catalog.model.LightSaber
import java.time.LocalDate
import java.time.Month
import javax.enterprise.context.ApplicationScoped

@ApplicationScoped
class GalaxyService(
    private val heroes: MutableList<Hero> = mutableListOf(),
    private val films: MutableList<Film> = mutableListOf()
) {

    init {
        films.add(Film("A New Hope", 4, "George Lucas", LocalDate.of(1977, Month.MAY, 21)))
        films.add(Film("The Empire Strikes Back", 5, "George Lucas", LocalDate.of(1980, Month.MAY, 21)))
        films.add(Film("Return Of The Jedi", 6, "George Lucas", LocalDate.of(1983, Month.MAY, 25)))

        heroes.add(Hero("Luke", "Skywalker", 1.7, 73, false, LightSaber.GREEN, listOf(4, 5, 6)))
        heroes.add(Hero("Leia", "Organa", 1.5, 51, false, null, listOf(4, 5, 6)))
        heroes.add(Hero("Darth", "Vader", 1.9, 89, true, LightSaber.RED, listOf(4, 5, 6)))
    }

    fun getAllFilms() = films

    fun getFilm(id: Int) = films[id]

    fun getHeroesByFilm(film: Film) = heroes.filter { it.episodeIds.contains(film.episodeID) }

    fun addHero(hero: Hero) {
        heroes.add(hero)
    }

    fun deleteHero(id: Int) = heroes.removeAt(id)

    fun getHeroesBySurname(surname: String) = heroes.filter { it.surname == surname }

}